# Capstone project

Visit running application on : https://capstone.au-syd.mybluemix.net/

## Building the application

### Prerequisites

- [Git](https://git-scm.com/)
- [Node.js and npm](nodejs.org) Node >= 4.x.x, npm >= 2.x.x
- [Gulp](http://gulpjs.com/) (`npm install --global gulp`)
- [MongoDB](https://www.mongodb.org/) - Keep a running daemon with `mongod`

### Developing

1. Run `npm install` to install server dependencies.

2. Run `mongod` in a separate shell to keep an instance of the MongoDB Daemon running

3. Run `gulp serve` to start the development server. It should automatically open the client in your browser when ready.

## Build & development

Run `gulp build` for building and `gulp serve` for preview.

## Testing

Running `npm test` will run the unit tests with karma.

## Development log

- Decided to put my bets on yeoman for scaffolding
- Decided to put my bets on angular-fullstack for scafolding Angular client and server apps
- Well organized, but took some time to get to know the environment
- Added menu and routes for places, tickets. Users come out of the box.
- Had struggles with 'npm install' complaining about not being able to unlike a file. Solution was to downgrade npm to version 5.3
- Adding places:
  - Added a data folder to put in some sample data
  - Created some sample data
  - Figured out how to import data into mongodb using mongoimport
  - At first, data did not show up at the end point /api/places. It took quite a while to figure out the cause.
  - Found out data for development/test purposes can be inserted trough server\config\seed.js as well
- Deployed the application on IBM Bluemix and configured MongoDB cloud server
