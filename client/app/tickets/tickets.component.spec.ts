'use strict';

describe('Component: TicketsComponent', function() {
  // load the controller's module
  beforeEach(module('capstoneApp.tickets'));

  var TicketsComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    TicketsComponent = $componentController('tickets', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
