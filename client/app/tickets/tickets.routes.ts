'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('tickets', {
      url: '/tickets',
      template: '<tickets></tickets>'
    });
}
