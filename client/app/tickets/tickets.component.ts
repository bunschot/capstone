'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './tickets.routes';

export class TicketsComponent {
  $http;

  tickets = [];

  /*@ngInject*/
  constructor($http) {
    this.$http = $http;
  }

  $onInit() {
    this.$http.get('/api/tickets').then(response => {
      this.tickets = response.data;
    });
  }
}

export default angular.module('capstoneApp.tickets', [uiRouter])
  .config(routes)
  .component('tickets', {
    template: require('./tickets.html'),
    controller: TicketsComponent,
    controllerAs: 'ticketsCtrl'
  })
  .name;
