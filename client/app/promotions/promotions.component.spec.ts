'use strict';

describe('Component: PromotionsComponent', function() {
  // load the controller's module
  beforeEach(module('capstoneApp.promotions'));

  var PromotionsComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    PromotionsComponent = $componentController('promotions', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
