'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './promotions.routes';

export class PromotionsComponent {
  /*@ngInject*/

  message = '';

  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('capstoneApp.promotions', [uiRouter])
  .config(routes)
  .component('promotions', {
    template: require('./promotions.html'),
    controller: PromotionsComponent,
    controllerAs: 'promotionsCtrl'
  })
  .name;
