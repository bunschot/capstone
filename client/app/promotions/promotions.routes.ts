'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('promotions', {
      url: '/promotions',
      template: '<promotions></promotions>'
    });
}
