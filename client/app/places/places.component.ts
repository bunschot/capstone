'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './places.routes';

export class PlacesComponent {
  $http;

  places = [];

  /*@ngInject*/
  constructor($http) {
    this.$http = $http;
  }

  $onInit() {
    this.$http.get('/api/places').then(response => {
      this.places = response.data;
    });
  }
}

export default angular.module('capstoneApp.places', [uiRouter])
  .config(routes)
  .component('places', {
    template: require('./places.html'),
    controller: PlacesComponent,
    controllerAs: 'placesCtrl'
  })
  .name;
