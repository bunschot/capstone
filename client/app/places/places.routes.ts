'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('places', {
      url: '/places',
      template: '<places></places>'
    });
}
