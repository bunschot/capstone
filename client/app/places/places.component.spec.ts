'use strict';

describe('Component: PlacesComponent', function() {
  // load the controller's module
  beforeEach(module('capstoneApp.places'));

  var PlacesComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    PlacesComponent = $componentController('places', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
