'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './place.events';

var PlaceSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  image: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  active: {
    type: Boolean,
    default: true
  }
}, {
    timestamps: true
});

registerEvents(PlaceSchema);
export default mongoose.model('Place', PlaceSchema);
