'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var placeCtrlStub = {
  index: 'placeCtrl.index',
  show: 'placeCtrl.show',
  create: 'placeCtrl.create',
  upsert: 'placeCtrl.upsert',
  patch: 'placeCtrl.patch',
  destroy: 'placeCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var placeIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './place.controller': placeCtrlStub
});

describe('Place API Router:', function() {
  it('should return an express router instance', function() {
    expect(placeIndex).to.equal(routerStub);
  });

  describe('GET /api/places', function() {
    it('should route to place.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'placeCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/places/:id', function() {
    it('should route to place.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'placeCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/places', function() {
    it('should route to place.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'placeCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/places/:id', function() {
    it('should route to place.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'placeCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/places/:id', function() {
    it('should route to place.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'placeCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/places/:id', function() {
    it('should route to place.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'placeCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
