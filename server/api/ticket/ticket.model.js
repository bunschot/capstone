'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './ticket.events';

import User from '../user/user.model';
import Place from '../place/place.model';

var TicketSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  place: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Place'
  },
  code: {
    type: String,
    required: true,
    unique: true
  },
  status: {
    type: String,
    default: '',
  },
  active: {
    type: Boolean,
    default: true
  }
}, {
  timestamps: true
});

registerEvents(TicketSchema);
export default mongoose.model('Ticket', TicketSchema);
